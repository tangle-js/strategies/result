const Strategy = require('@tangle/strategy')
const Validator = require('is-my-json-valid')

module.exports = function Result (composition = {}, isError) {
  if (typeof isError !== 'function') throw new Error('isError must be defined')
  const strategy = new Strategy(composition)

  const schema = {
    type: 'object',
    properties: {
      ok: strategy.schema
    },
    required: ['ok'],
    additionalProperties: false
  }
  const isValid = Validator(schema, { verbose: true })

  return {
    schema,
    isValid (T) {
      this.isValid.error = null // wipe any last error recorded
      const result = isValid(T)
      if (!result) this.isValid.error = isValid.error

      // QUESTION - do we want isError in here?
      if (isError(T.ok)) return false

      return result
    },

    identity () {
      return {
        ok: {}
      }
    },

    mapFromInput (input) {
      return {
        ok: strategy.mapFromInput({}, input)
      }
    },

    mapToOutput (T) {
      if (T.error) return T.error
      return strategy.mapToOutput(T.ok)
    },

    concat (A, B) {
      if (A.error || B.error) return { error: A.error + B.error }

      const result = strategy.concat(A.ok, B.ok)

      if (isError(result)) return { error: isError.error || 'invalid concat' }
      else return { ok: result }
    },

    // LATER
    isConflict (A, B) {
    },
    isValidMerge (A, B) {
    },
    merge (A, B) {
    }
  }
}

// const profile = Result({
//   preferredName: Overwrite(),
//   description: Overwrite()
// })

// if (!Strategy.isValid(profile)) {
//   console.log(Strategy.isValid.error)
// }

// let result

// result = profile.concat(
//   profile.mapFromInput({
//     author: 'dave',
//     preferredName: 'mix'
//   }),
//   profile.mapFromInput({
//     preferredName: 'mixxy'
//   })
// )

// console.log(result)
