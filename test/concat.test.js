const test = require('tape')
const Result = require('../')
const mock = require('./mock-composition')

const ComplexSet = require('@tangle/complex-set')
const { mapToOutput } = ComplexSet()

test('concat (simple)', t => {
  const result = Result(mock, () => false)

  const okA = {
    ok: {
      title: { set: 'party' }
    }
  }
  const okB = {
    ok: {
      title: { set: 'dinner' }
    }
  }
  const err = {
    error: new Error('no')
  }

  t.true('ok' in result.concat(okA, okB)) // could be err, but we don't detect any errors in this strategy
  t.true('error' in result.concat(okA, err))
  t.true('error' in result.concat(err, okA))
  t.true('error' in result.concat(err, err))

  t.end()
})

test('concat (authors scenario)', t => {
  function isError (ok) {
    isError.error = null

    const ranges = mapToOutput(ok.authors)

    for (const [author, seqs] of Object.entries(ok.authorHistory)) {
      if (!(author in ok.authors)) {
        isError.error = new Error(`author is not in authors: ${author}`)
        return true
      }

      const problemSeq = seqs.find(seq => {
        const validRange = ranges[author].find(range => {
          return (
            seq >= range.start &&
            (range.end === null || seq <= range.end) // TODO check if end is inclusive (<=)
          )
        })
        return !validRange
      })

      if (problemSeq) {
        isError.error = new Error(`${author} is a valid author, but not for sequence ${problemSeq}`)
        return true // found error
      }
    }

    return false // no errors !
  }

  const result = Result(mock, isError)

  const initial = {
    ok: {
      authors: {
        '@mix': { 20: 1, 53: -1 }, // messages in mix's sequence range 20-53 inclusive are valid
        '@colin': { 40: 1 }
      }
    }
  }

  let expected, actual

  actual = result.concat(
    initial,
    {
      ok: {
        authorHistory: {
          '@mix': [40]
        },
        title: { set: 'hooray' }
      }
    }
  )
  expected = {
    ok: {
      authors: {
        '@mix': { 20: 1, 53: -1 }, // messages in mix's sequence range 20-53 inclusive are valid
        '@colin': { 40: 1 }
      },
      authorHistory: {
        '@mix': [40]
      },
      title: { set: 'hooray' }
    }
  }
  t.deepEqual(actual, expected, 'known author in range contribution = ok')

  actual = result.concat(
    initial,
    {
      ok: {
        authorHistory: {
          '@random': [60]
        },
        title: { set: 'hooray' }
      }
    }
  )
  expected = { error: new Error('author is not in authors: @random') }
  t.deepEqual(actual, expected, 'unknown author = error')

  actual = result.concat(
    initial,
    {
      ok: {
        authorHistory: {
          '@mix': [60]
        },
        title: { set: 'hooray' }
      }
    }
  )
  expected = { error: new Error('@mix is a valid author, but not for sequence 60') }
  t.deepEqual(actual, expected, 'author out of range = error')

  t.end()
})
