const Overwrite = require('@tangle/overwrite')
const OverwriteFields = require('@tangle/overwrite-fields')
const ComplexSet = require('@tangle/complex-set')

module.exports = {
  title: Overwrite(),

  authorHistory: OverwriteFields(), // poor choice
  authors: ComplexSet() // valid authors
}
