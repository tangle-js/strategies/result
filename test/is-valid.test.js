const test = require('tape')
const Overwrite = require('@tangle/overwrite')
const Result = require('../')

test('isValid', t => {
  let result = Result({}, () => false)
  t.true(result.isValid({
    ok: {}
  }))
  t.false(result.isValid({
    error: {}
  }))

  result = Result(
    { count: Overwrite({ valueSchema: { type: 'number' } }) },
    ok => ok.count.set === 4
  )
  t.true(
    result.isValid({
      ok: {
        count: { set: 3 }
      }
    }),
    'is valid if count is not 4'
  )
  t.false(
    result.isValid({
      ok: {
        count: { set: 4 }
      }
    }),
    'is not valid if count 4'
  )

  t.end()
})
