const Strategy = require('@tangle/strategy')
const test = require('tape')

const Result = require('../')
const mock = require('./mock-composition')

test('init', t => {
  let result = Result({}, () => false)
  let isValid = Strategy.isValid(result)
  t.true(isValid, 'makes a valid strategy (empty)')
  if (!isValid) console.log(Strategy.isValid.error)

  result = Result(mock, () => false)
  isValid = Strategy.isValid(result)
  t.true(isValid, 'makes a valid strategy (with composition)')
  if (!isValid) console.log(Strategy.isValid.error)

  t.end()
})
